<div align="center">

  <h1>Traffic signs detection</h1>

  <a href="https://www.python.org/"><img alt="Python" src="https://img.shields.io/badge/Python-3.7-%23404d59.svg?logo=Python&logoColor=white" height="20"/></a>
  <a href="https://www.tensorflow.org/"><img alt="TensorFlow" src="https://img.shields.io/badge/TensorFlow-2.1.0-%23404d59.svg?logo=TensorFlow&logoColor=white" height="20"/></a>

  <p><b>Deep learning project to detect and classify traffic signs from given images.</b></p>

  </br>

  <sub>University project which allows to train convolutional neural network model and show its prediction on given images. It uses sliding window and image pyramid techniques to find a traffic sign on the image.<sub>

</div>


![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Installation

Install Python 3.7 and following libraries:
* numpy==1.18.1
* matplotlib==3.1.3
* opencv==3.4.2
* tensorflow==2.1.0
* tqdm==4.46.0
* tabulate==0.8.3

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Usage

There are three scripts which you may use:
* `train.py` to train a new model on images from `random_data` directory;
* `object_detector.py` to find traffic sign on the image, crop it and save to `output.jpg` file;
* `classify.py` to check neural network model predictions on given image.

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Results

![znaki1_007](https://user-images.githubusercontent.com/49961031/110140990-f9a7e200-7dd4-11eb-875a-e02ae2925d74.jpg)
![znaki4_002](https://user-images.githubusercontent.com/49961031/110141036-07f5fe00-7dd5-11eb-94b0-13c8d261e6d0.jpg)
![znaki5_002](https://user-images.githubusercontent.com/49961031/110141042-09272b00-7dd5-11eb-83ec-4daaa0479e63.jpg)

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## Credit

[Michał Kliczkowski](https://github.com/michal090497) - co-developer

![split](https://github.com/terkelg/prompts/raw/master/media/split.png)

## License

Licensed under [MIT](https://opensource.org/license/mit/).
